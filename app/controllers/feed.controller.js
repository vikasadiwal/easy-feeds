const Feed = require('../models/feed.model.js');

// Create and Save a new feed
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    return res.status(400).send({
      message: "Feed name can not be empty"
    });
  }

  // Create a Feed
  const feed = new Feed({
    name: req.body.name,
    description: req.body.description,
    link: req.body.link,
    price: req.body.price
  });

  // Save Feed in the database
  feed.save()
    .then(data => {
      res.send(data);
    }).catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Feed."
      });
    });
};

// Retrieve and return all feeds from the database.
exports.findAll = (req, res) => {
  Feed.find()
    .then(feeds => {
      res.send(feeds);
    }).catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving feeds."
      });
    });
};

// Find a single feed with a feedId
exports.findOne = (req, res) => {
  feeds.findById(req.params.feedId)
    .then(feeds => {
      if (!feeds) {
        return res.status(404).send({
          message: "feeds not found with id " + req.params.feedId
        });
      }
      res.send(feeds);
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: "feeds not found with id " + req.params.feedId
        });
      }
      return res.status(500).send({
        message: "Error retrieving feeds with id " + req.params.feedId
      });
    });
};

// Update a feed identified by the feedId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body.name) {
    return res.status(400).send({
      message: "Feed name can not be empty"
    });
  }

  // Find feedId and update it with the request body
  Feed.findByIdAndUpdate(req.params.feedId, {
      title: req.body.title || "Untitled Feed",
      content: req.body.content
    }, {
      new: true
    })
    .then(feedId => {
      if (!feedId) {
        return res.status(404).send({
          message: "Feed not found with id " + req.params.feedId
        });
      }
      res.send(feedId);
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: "Feed not found with id " + req.params.feedId
        });
      }
      return res.status(500).send({
        message: "Error updating feedId with id " + req.params.feedId
      });
    });
};

// Delete a feed with the specified feedId in the request
exports.delete = (req, res) => {
  Feed.findByIdAndRemove(req.params.feedId)
    .then(note => {
      if (!note) {
        return res.status(404).send({
          message: "Feed not found with id " + req.params.feedId
        });
      }
      res.send({
        message: "Feed deleted successfully!"
      });
    }).catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: "Feed not found with id " + req.params.feedId
        });
      }
      return res.status(500).send({
        message: "Could not delete note with id " + req.params.feedId
      });
    });
};