module.exports = (app) => {
  const feeds = require('../controllers/feed.controller.js');

  // Create a new Feed
  app.post('/feeds', feeds.create);

  // Retrieve all Feeds
  app.get('/feeds', feeds.findAll);

  // Retrieve a single Feed with feedId
  app.get('/feeds/:feedId', feeds.findOne);

  // Update a Feed with feedId
  app.put('/feeds/:noteId', feeds.update);

  // Delete a Feed with feedId
  app.delete('/feeds/:feedId', feeds.delete);
}