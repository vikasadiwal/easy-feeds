const mongoose = require('mongoose');

const FeedSchema = mongoose.Schema({
  name: String,
  description: String,
  link: String,
  price: Number
}, {
  timestamps: true
});

module.exports = mongoose.model('Feeds', FeedSchema);
